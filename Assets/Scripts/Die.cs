﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : MonoBehaviour {

	public int health;
	public Animator anim;

	// Use this for initialization
	void OnCollisionEnter2D () {
		health -= 3;
	}

	// Update is called once per frame
	void Update () {
		if(health <= 0) {
			anim.SetTrigger("Die");
		}
		else {
			anim.SetBool("Die", false);
		}
	}
}
