using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Shoot : MonoBehaviour {

	//Bullet preafab
	public GameObject projectilePrefab;

	//Reloading float and int and text and bool
	public int maxAmmo;
	public int currentAmmo;

	public float reloadTime;

	public Text ammoText;

	public bool isReloading = false;

	//
	//public ParticleSystem muzzleFlash;

	//List of all the bullets in the current instants
	public static List<GameObject> Projectiles = new List<GameObject>();

	public Transform bulletSpawn;

	[Range(0, 100)]
	public float projectileVelocity;

	// Use this for initialization
	void Start () {
		currentAmmo = maxAmmo;
		projectileVelocity = 30;
	}

	// Update is called once per frame
	void Update () {

		ammoText.text = currentAmmo + "/" + maxAmmo;

		if(currentAmmo <= 0 && isReloading == false) {
			StartCoroutine("Reload");
		}

		if(Input.GetButton("Fire1") && currentAmmo > 0) {
			GameObject bullet = (GameObject)Instantiate(projectilePrefab, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
			//muzzleFlash.Play();
			currentAmmo--;
			Projectiles.Add(bullet);
		}

		for(int i = 0; i < Projectiles.Count; i++) {
			GameObject goBullet = Projectiles[i];

			if(goBullet != null) {
				goBullet.transform.Translate(new Vector3(0, 1) * Time.deltaTime * projectileVelocity);
			}
		}
	}

	IEnumerator Reload() {
		isReloading = true;
		yield return new WaitForSeconds(reloadTime);
		currentAmmo = maxAmmo;
		isReloading = false;
	}
}
