using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public Animator anim;

    // Local Variables
    bool grounded = true;

	bool isJumping() {
		return Input.GetKeyDown (KeyCode.Space)   ||
			   Input.GetKeyDown (KeyCode.UpArrow) ||
			   Input.GetKeyDown (KeyCode.Z)       ||
			   Input.GetKeyDown (KeyCode.W);
		}
	Rigidbody2D rb;

	//Movement
	public float speed = 10f;
	public float jump = 5f;
	float moveVelocity;

	void Awake() {
		rb = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		//Jumping
		if (isJumping())
		{
			if(grounded == true)
			{
				rb.velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, jump);
				grounded = false;
			}
		}

		// Reset movement velocity every frame
		moveVelocity = 0;

		//Left Right Movement
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A))
			moveVelocity = -speed;


		if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D))
		{

			moveVelocity = speed;
			// Apply gravity to player if they aren't wall-grabbing
			// to prevent them from getting stuck
			if (rb.velocity.x == 0 && isJumping()) {
				rb.velocity = new Vector2(0, -speed);
			}
		}

			rb.velocity = new Vector2 (moveVelocity, rb.velocity.y);
	}

	//Check if Grounded
	void OnCollisionEnter2D()
	{
		grounded = true;
	}

	void OnCollisionStay2D() {
		grounded = true;
	}

	void OnCollisionExit2D()
	{
		grounded = false;
	}
}
