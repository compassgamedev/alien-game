using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullet : MonoBehaviour {

	public List<GameObject> Projectiles = new List<GameObject>();

	void Update() {
		Projectiles = Shoot.Projectiles;
	}

	void OnCollisionEnter2D(Collision2D col) {
			Destroy(gameObject);
	}
}
