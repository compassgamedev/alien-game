﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

	public Animator anim;

	void Start() {
		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D() {
		anim.SetBool("IsOpen", true);
	}

	void OnTriggerExit2D() {
		anim.SetBool("IsOpen", false);
	}
}
