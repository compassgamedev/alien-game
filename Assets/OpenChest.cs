using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChest : MonoBehaviour {

	public Animator anim;

	void Start() {
		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D() {
		anim.SetBool("IsOpen", true);
	}
}
